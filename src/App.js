import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import Main from "./components/Main";

import axios from "axios";
import { v4 as uuidv4 } from "uuid";

class App extends Component {
  state = {
    displayLeftSide: false,
    data: [],
    isLoaded: false,
    userInput: "",
    show: false,
    deleteshow: false,
    editProjectShow: false,
    searchInputValue: "",
    filterShow: false,
    showEditProject: false,
    favorite: false,
    colorValue: "",
    colors:{30:"#b8256f",31:"#db4035",32:"#ff9933", 36:"#299438", 47:"#808080", 48:"#b8b8b8"},
    
  };
  hanldeColor = (e)=>{
    let color = e.target.value
    this.setState({
      colorValue: color
    })
    console.log(this.state.colorValue)
  }
  handleAddtofavorite = () =>{
    this.setState({
      favorite: !this.state.favorite
    })
    console.log(this.state.favorite)
  }
  handleHideLeftComponent = () =>{
    this.setState({
      displayLeftSide: !this.state.displayLeftSide,
    })
  }
  handleChange = (e) => {
    this.setState({
      userInput: e.target.value,
    });
  };
  handleClose = () => {
    this.setState({
      show: false,
    });
  };
  handleDeleteClose = () => {
    this.setState({
      deleteshow: false,
    });
  };
  handleEditProjectClose = () => {
    this.setState({
      showEditProject: false,
    });
  };
  handleAddproject = async () => {
    console.log("hi");
    try {
      if (this.state.userInput !== "") {
        const article = { name: `${this.state.userInput}`, favorite:this.state.favorite, color: parseInt(this.state.colorValue) };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          "https://api.todoist.com/rest/v1/projects",
          article,
          { headers }
        );
        this.setState({
          data: [...this.state.data, response.data],
          show: false,
          // userInput: ""
        });
      }
    } catch (error) {
      console.error(error);
    }
  };
  handleEditProject = async (id) => {
    console.log("Edit project");
    console.log(id)
    console.log(this.state.userInput)
    try {
      if (this.state.userInput !== "") {
        const article = { name: `${this.state.userInput}` };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          `https://api.todoist.com/rest/v1/projects/${id}`,
          article,
          { headers }
        );
        let updatedData1 = this.state.data.map((each)=> {
          if(each.id === id){
            each.name = this.state.userInput
            return each
          }
          else {
            return each
          }
        })
        this.setState({
          data: updatedData1,
          showEditProject: false,
          // userInput: ""
        });
      }
    } catch (error) {
      console.error(error);
    }
  };
  handleAddToFavorites = async(id) =>{
    const article = { favorite: true };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          `https://api.todoist.com/rest/v1/projects/${id}`,
          article,
          { headers }
        );
        
        this.setState({
          data: this.state.data.map((project) => {
            if (project.id === id) {
              return {
                ...project,
                favorite: !project.favorite,
              };
            }
            return project;
          }),
        });
        console.log(!this.state.favorite)
  }
  handleRemoveFavorites = async(each) =>{
    console.log(each.id)
    const article = { favorite: false, name: `${each.name}` };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          `https://api.todoist.com/rest/v1/projects/${each.id}`,
          article,
          { headers }
        );
        
        this.setState({
          data: this.state.data.map((project) => {
            if (project.id === each.id) {
              return {
                ...project,
                favorite: !project.favorite,
              };
            }
            return project;
          }),
        });
  }
  handleDelete = async (id) => {
    console.log(id);
    const headers = {
      Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
    };
    axios
      .delete(`https://api.todoist.com/rest/v1/projects/${id}`, { headers })
      .then(() => {
        let updatedData = this.state.data.filter((task) => {
          if (task.id !== id) {
            return task;
          } else {
            return null;
          }
        });
        this.setState({
          data: updatedData,
          deleteshow: false,
        });
      });
  };
  handleShow = (e) => {
    this.setState({
      show: true,
    });
  };
  handleDeleteShow = (e) => {
    console.log("hi")
    this.setState({
      deleteshow: true,
    });
  };
  handleEditProjectShow = (e) => {
    console.log("hi")
    this.setState({
      showEditProject: true,
    });
  };
  handleSearchFIlter = (e)=>{
    console.log('onChange Event Triggered')
    this.setState({
      searchInputValue: e.target.value,
    });
    console.log(this.state.searchInputValue)
  }
  handleInputElClick = () => {
    this.setState({
      filterShow: true
    })
    console.log(this.state.filterShow)
  }
  handleFilteredTasks = () => {
    const filteredTasks = this.state.data.filter((each) => 
      each.name.toLowerCase().includes(this.state.searchInputValue)
    );
    if (filteredTasks && this.state.searchInputValue) {
      return filteredTasks;
    } else {
      return [];
    }
  };
  handleGetProject = () =>{
    console.log('hi')
  }
  filterFavoriteProjects = () =>{
    let filteredFvaoriteProjects = this.state.data.filter(
      (each) =>
        each.favorite === true
    );
    if (filteredFvaoriteProjects) {
      return filteredFvaoriteProjects;
    } else {
      return [];
    }
  }
  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
      },
    };
    axios
      .get(`https://api.todoist.com/rest/v1/projects`, options)
      .then((resp) => this.setState({ data: resp.data, isLoaded: true }));
  }
  render() {
    console.log(this.state.data)
    
    
    return (
      <BrowserRouter>
        <Header
        handleHideLeftComponent = {this.handleHideLeftComponent} 
        handleInputElClick = {this.handleInputElClick}
        handleSearchFIlter = {this.handleSearchFIlter}
        searchInputValue = {this.state.searchInputValue}
        handleFilteredTasks = {this.handleFilteredTasks}
        handleGetProject = {this.handleGetProject}
        filterShow = {this.state.filterShow}
        />
        <Main  
         displayLeftSide = {this.state.displayLeftSide}
         handleHideLeftComponent = {this.handleHideLeftComponent}
         handleChange = {this.handleChange}
         handleClose = {this.handleClose}
         handleDeleteClose = {this.handleDeleteClose}
         handleAddproject = {this.handleAddproject}
         handleDelete = {this.handleDelete}
         handleShow = {this.handleShow}
         handleDeleteShow = {this.handleDeleteShow}
         data = {this.state.data}
         isLoaded = {this.state.isLoaded}
         deleteshow={this.state.deleteshow}
         show={this.state.show}
         showEditProject = {this.state.showEditProject}
         handleEditProjectClose = {this.handleEditProjectClose}
         handleEditProjectShow = {this.handleEditProjectShow}
         handleEditProject= {this.handleEditProject}
         handleAddToFavorites={this.handleAddToFavorites}
         handleRemoveFavorites={this.handleRemoveFavorites}
         filterFavoriteProjects={this.filterFavoriteProjects}
         handleAddtofavorite={this.handleAddtofavorite}
         hanldeColor={this.hanldeColor}
         colorValue={this.state.colorValue}
         colors={this.state.colors}
         />
      </BrowserRouter>
    );
  }
}

export default App;
