import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Row, Col, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Accordion, Dropdown, Modal, Button } from "react-bootstrap";
// import axios from "axios";
// import { v4 as uuidv4 } from "uuid";
import RightSideComponent from "./RightsideComponent";
import PopupComponent from "./PopupComponent";
import TodayComponent from "./TodayComponent";
import UpcomingComponent from "./UpcomingComponent";
class Main extends Component {
  render() {
    const filterFavoriteProjects = this.props.filterFavoriteProjects();
    console.log(filterFavoriteProjects);
    console.log(this.props);
    console.log(this.props);
    return (
      <Container fluid>
        <Row>
          {this.props.displayLeftSide ? null : (
            <Col
              className={
                this.props.displayLeftSide
                  ? "d-none d-lg-block d-md-block"
                  : "d-lg-block d-md-block"
              }
              style={{
                backgroundColor: "#fafafa",
                height: "100vh",
                width: "100vw",
              }}
            >
              <div className="d-flex flex-column project-container">
                <Link
                  to={{
                    pathname: `/projects/app/today`,
                  }}
                >
                  <div className="mt-3 mb-3 d-flex">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="green"
                      class="bi bi-calendar2-date"
                      viewBox="0 0 16 16"
                    >
                      <path d="M6.445 12.688V7.354h-.633A12.6 12.6 0 0 0 4.5 8.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
                      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                      <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                    </svg>
                    <div className="project-name">Today</div>
                    
                  </div>
                </Link>
                <Link
                  to={{
                    pathname: `/projects/app/upcoming`,
                  }}
                >
                  <div className="mb-3 d-flex">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="blue"
                      class="bi bi-calendar4-week"
                      viewBox="0 0 16 16"
                    >
                      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V5z" />
                      <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm-2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
                    </svg>
                    <div className="project-name">Upcoming</div>
                    
                  </div>
                </Link>
                <Accordion className="accordion-style">
                  <Accordion.Item>
                    <Accordion.Header>Favorites</Accordion.Header>
                    <Accordion.Body>
                      {this.props.isLoaded
                        ? filterFavoriteProjects.map((each, index) => {
                            // console.log(each.id);
                            return (
                              <div className="d-flex">
                                <Link
                                  className="diplay"
                                  to={{
                                    pathname: `/projects/${each.id}`,
                                    item: { data: { each } },
                                  }}
                                >
                                  <div className="d-flex">
                                  <div>
                                        <svg
                                          className="circle-icon"
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="16"
                                          height="16"
                                          fill={`${
                                            this.props.colors[each.color]
                                          }`}
                                          class="bi bi-circle-fill"
                                          viewBox="0 0 16 16"
                                        >
                                          <circle cx="8" cy="8" r="8" />
                                        </svg>
                                      </div>
                                      <div className="project-name">{each.name}</div>
                                  </div>
                                </Link>
                                <Dropdown className="delete-button">
                                  <Dropdown.Toggle
                                    className="toggle-background-color"
                                    id="dropdown-basic"
                                  >
                                    . . .
                                  </Dropdown.Toggle>

                                  <Dropdown.Menu>
                                    <Dropdown.Item>
                                      <Modal
                                        show={this.props.showEditProject}
                                        onHide={
                                          this.props.handleEditProjectClose
                                        }
                                        animation={false}
                                      >
                                        <Modal.Header closeButton>
                                          <Modal.Title></Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                          <h3>Name</h3>
                                          <input
                                            onChange={this.props.handleChange}
                                            type="text"
                                          />
                                          <h3>Color</h3>
                                          <select
                                            className="color-change"
                                            onChange={this.hanldeColor}
                                            ref={(input) => (this.menu = input)}
                                          >
                                            <option value="47">Charcoal</option>
                                            <option value="30">
                                              Berry Red
                                            </option>
                                            <option value="31">Red</option>
                                            <option value="36">Green</option>
                                            <option value="32">Orange</option>
                                            <option value="48">Grey</option>
                                          </select>
                                          <div>
                                            <label class="switch">
                                              <input type="checkbox" />
                                              <span class="slider round"></span>
                                            </label>
                                          </div>
                                        </Modal.Body>
                                        <Modal.Footer>
                                          <Button
                                            variant="secondary"
                                            onClick={
                                              this.props.handleEditProjectClose
                                            }
                                          >
                                            Cancel
                                          </Button>
                                          <Button
                                            variant="primary"
                                            onClick={() =>
                                              this.props.handleEditProject(
                                                each.id
                                              )
                                            }
                                          >
                                            Save
                                          </Button>
                                        </Modal.Footer>
                                      </Modal>
                                      <p
                                        className="popup-button"
                                        onClick={
                                          this.props.handleEditProjectShow
                                        }
                                      >
                                        Edit project
                                      </p>
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                      onClick={() => {
                                        this.props.handleRemoveFavorites(each);
                                      }}
                                    >
                                      Remove from favorites
                                    </Dropdown.Item>
                                  </Dropdown.Menu>
                                </Dropdown>
                              </div>
                            );
                          })
                        : "...isloading"}
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>
                <div className="d-flex">
                  <Accordion className="accordion-style">
                    <Accordion.Item>
                      <Accordion.Header>Projects</Accordion.Header>
                      <Accordion.Body>
                        {this.props.isLoaded
                          ? this.props.data.map((each, index) => {
                              console.log(each.color);
                              console.log(this.props.colors[each.color]);
                              return (
                                <div className="d-flex">
                                  <Link
                                    className="diplay"
                                    to={{
                                      pathname: `/projects/${each.id}`,
                                      item: { data: { each } },
                                    }}
                                  >
                                    <div key={index} className="d-flex">
                                      <div>
                                        <svg
                                          className="circle-icon"
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="16"
                                          height="16"
                                          fill={`${
                                            this.props.colors[each.color]
                                          }`}
                                          class="bi bi-circle-fill"
                                          viewBox="0 0 16 16"
                                        >
                                          <circle cx="8" cy="8" r="8" />
                                        </svg>
                                      </div>
                                      <div className="project-name">{each.name}</div>
                                    </div>
                                  </Link>
                                  <Dropdown className="delete-button">
                                    <Dropdown.Toggle
                                      className="toggle-background-color"
                                      id="dropdown-basic"
                                    >
                                      . . .
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                      {/* <Dropdown.Item>
                                        Add project above
                                      </Dropdown.Item>
                                      <Dropdown.Item>
                                        Add project below
                                      </Dropdown.Item>
                                      <hr /> */}
                                      <Dropdown.Item>
                                        <Modal
                                          show={this.props.showEditProject}
                                          onHide={
                                            this.props.handleEditProjectClose
                                          }
                                          animation={false}
                                        >
                                          <Modal.Header closeButton>
                                            <Modal.Title></Modal.Title>
                                          </Modal.Header>
                                          <Modal.Body>
                                            <h3>Name</h3>
                                            <input
                                              className="text-input"
                                              onChange={this.props.handleChange}
                                              type="text"
                                            />
                                            <h3>Color</h3>
                                            <select
                                              className="color-change mb-3"
                                              ref={(input) =>
                                                (this.menu = input)
                                              }
                                            >
                                              <option value="Charcoal">
                                                Charcoal
                                              </option>
                                              <option value="Berry Red">
                                                Berry Red
                                              </option>
                                              <option value="Red">Red</option>
                                              <option value="Green">
                                                Green
                                              </option>
                                              <option value="Orange">
                                                Orange
                                              </option>
                                              <option value="Grey">Grey</option>
                                            </select>
                                            <div>
                                              <label class="switch">
                                                <input type="checkbox" />
                                                <span class="slider round"></span>
                                              </label>
                                              Add to favorites
                                            </div>
                                          </Modal.Body>
                                          <Modal.Footer>
                                            <Button
                                              variant="secondary"
                                              onClick={
                                                this.props
                                                  .handleEditProjectClose
                                              }
                                            >
                                              Cancel
                                            </Button>
                                            <Button
                                              variant="primary"
                                              onClick={() =>
                                                this.props.handleEditProject(
                                                  each.id
                                                )
                                              }
                                            >
                                              Save
                                            </Button>
                                          </Modal.Footer>
                                        </Modal>
                                        <p
                                          className="popup-button"
                                          onClick={
                                            this.props.handleEditProjectShow
                                          }
                                        >
                                          Edit project
                                        </p>
                                      </Dropdown.Item>
                                      {/* <Dropdown.Item>
                                        Share project
                                      </Dropdown.Item> */}
                                      <Dropdown.Item
                                        onClick={() => {
                                          this.props.handleAddToFavorites(
                                            each.id
                                          );
                                        }}
                                      >
                                        Add to favorites
                                      </Dropdown.Item>
                                      {/* <Dropdown.Item>
                                        Another action
                                      </Dropdown.Item>
                                      <hr />
                                      <Dropdown.Item>
                                        Duplicate project
                                      </Dropdown.Item>
                                      <Dropdown.Item>
                                        Email tasks to this project
                                      </Dropdown.Item>
                                      <Dropdown.Item>
                                        Project calendar feed
                                      </Dropdown.Item>
                                      <hr />
                                      <Dropdown.Item>
                                        Archive project
                                      </Dropdown.Item> */}
                                      <Dropdown.Item>
                                        <Modal
                                          show={this.props.deleteshow}
                                          onHide={this.props.handleDeleteClose}
                                          animation={false}
                                        >
                                          <Modal.Header closeButton>
                                            <Modal.Title></Modal.Title>
                                          </Modal.Header>
                                          <Modal.Body>
                                            <h3>
                                              Are you sure you want to delete
                                              siva?
                                            </h3>
                                          </Modal.Body>
                                          <Modal.Footer>
                                            <Button
                                              variant="secondary"
                                              onClick={
                                                this.props.handleDeleteClose
                                              }
                                            >
                                              Cancel
                                            </Button>
                                            <Button
                                              variant="danger"
                                              onClick={() =>
                                                this.props.handleDelete(each.id)
                                              }
                                            >
                                              Delete Todo
                                            </Button>
                                          </Modal.Footer>
                                        </Modal>
                                        <p
                                          className="popup-button"
                                          onClick={this.props.handleDeleteShow}
                                        >
                                          Delete
                                        </p>
                                      </Dropdown.Item>
                                    </Dropdown.Menu>
                                  </Dropdown>
                                </div>
                              );
                            })
                          : "...isloading"}
                      </Accordion.Body>
                    </Accordion.Item>
                  </Accordion>
                  <div>
                    <PopupComponent
                      handleChange={this.props.handleChange}
                      handleClose={this.props.handleClose}
                      handleShow={this.props.handleShow}
                      handleAddproject={this.props.handleAddproject}
                      show={this.props.show}
                      showEditProject={this.props.showEditProject}
                      handleAddtofavorite={this.props.handleAddtofavorite}
                      hanldeColor={this.props.hanldeColor}
                      // userInput = {this.state.userInput}
                    />
                  </div>
                </div>
              </div>
            </Col>
          )}
          <Col
            xs={this.props.displayLeftSide ? 12 : 9}
            lg={this.props.displayLeftSide ? 12 : 9}
            md={this.props.displayLeftSide ? 12 : 9}
            sm={this.props.displayLeftSide ? 12 : 9}
            className={
              this.props.displayLeftSide
                ? "d-lg-flex d-md-flex right-side-container"
                : "d-none d-lg-flex d-md-flex right-side-container"
            }
          >
            <Switch>
              <Route
                exact
                path="/projects/:handle"
                component={(props) => <RightSideComponent {...props} />}
              />
              <Route
                exact
                path="/projects/app/today"
                component={(props) => <TodayComponent {...props} />}
              />
              <Route
                exact
                path="/projects/app/upcoming"
                component={(props) => <UpcomingComponent {...props} />}
              />
            </Switch>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Main;
