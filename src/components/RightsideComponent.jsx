import React, { Component } from "react";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";
import { Dropdown, Modal, Button } from "react-bootstrap";
import DateComponent from "./DateComponent";

class RightSideComponent extends Component {
  state = {
    data: [],
    displayAddTask: false,
    displayEditTask: false,
    inputValue: "",
    isChecked: false,
    deleteshow: false,
    deleteTaskshow: false,
    show: false,
    displayShowCompletedTask: false,
    showEditTaskId: "",
    startDate: new Date(),
  };
  handleClose = () => {
    this.setState({
      show: false,
    });
  };
  handleShow = () => {
    this.setState({
      show: true,
    });
  };
  handleDeleteClose = () => {
    this.setState({
      deleteshow: false,
    });
  };
  handleDeleteTaskClose = () => {
    this.setState({
      deleteTaskshow: false,
    });
  };
  handleDeleteShow = (e) => {
    this.setState({
      deleteshow: true,
    });
  };
  handleDeleteTaskShow = (e) => {
    this.setState({
      deleteTaskshow: true,
    });
  };

  handleDeleteTask = async (id) => {
    // console.log(id);
    // console.log(id);
    const headers = {
      Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
    };
    axios
      .delete(`https://api.todoist.com/rest/v1/tasks/${id}`, { headers })
      .then(() => {
        let updatedData = this.state.data.filter((task) => {
          if (task.id !== id) {
            return task;
          } else {
            return null;
          }
        });
        this.setState({
          data: updatedData,
          deleteTaskshow: false,
        });
      });
  };
  handleActivetask = () => {
    let filteredTask = this.state.data.filter(
      (each) =>
        each.project_id === this.props.location.item.data.each.id &&
        each.completed === false
    );
    if (filteredTask) {
      return filteredTask;
    } else {
      return [];
    }
  };
  handleCompletedtask = () => {
    let filteredTask = this.state.data.filter(
      (each) =>
        each.project_id === this.props.location.item.data.each.id &&
        each.completed !== false
    );
    if (filteredTask) {
      return filteredTask;
    } else {
      return [];
    }
  };
  handleShowAddTask = () => {
    this.setState({
      displayAddTask: true,
    });
  };
  handleCloseEditTask = ()=>{
    this.setState({
      showEditTaskId: ""
    })
  }
  handleShowEditTask = (task) => {
    console.log("before ", this.state.showEditTaskId);
    this.setState(
      {
        showEditTaskId: task.id,
      },
      () => {
        console.log("after ", this.state.showEditTaskId);
      }
    );
  };
  handleCloseAddTask = () => {
    this.setState({
      displayAddTask: false,
    });
  };
  handleTaskInput = (e) => {
    this.setState({
      inputValue: e.target.value,
    });
  };
  handleCheckbox = (id) => {
    const headers = {
      Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
    };
    let task = this.state.data.find((task) => task.id === id);
    let url = "";
    if (task.completed) {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/reopen`;
    } else {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/close`;
    }
    axios
      .post(url, null, {
        headers,
      })
      .then(() => {
        this.setState({
          data: this.state.data.map((task) => {
            if (task.id === id) {
              return {
                ...task,
                completed: !task.completed,
              };
            }
            return task;
          }),
        });
      });
  };
  handleAddTask = async (id) => {
    var event = new Date(this.state.startDate);

    let date = JSON.stringify(event);
    date = date.slice(1, 11);
    // console.log(date);
    // console.log(id);
    try {
      if (this.state.inputValue !== "") {
        // const article = { name: `${this.state.userInput}` };
        const article = {
          content: `${this.state.inputValue}`,
          project_id: `${id}`,
          due_lang: "en",
          due_date: date,
        };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          "https://api.todoist.com/rest/v1/tasks",
          article,
          { headers }
        );
        this.setState({
          data: [...this.state.data, response.data],
          inputValue: "",
        });
      }
    } catch (error) {
      console.error(error);
    }
  };
  handleSaveTask = async(id) =>{
    console.log(id)
    console.log(this.state.inputValue)
    const article = { content: `${this.state.inputValue}` };
        const headers = {
          Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          `https://api.todoist.com/rest/v1/tasks/${id}`,
          article,
          { headers }
        );
        
        let updatedData1 = this.state.data.map((each)=> {
          console.log(each.id)
          if(each.id === id){
            each.content = this.state.inputValue
            return each
          }
          else {
            return each
          }
        })
        this.setState({
          data: updatedData1,
          userInput: ""
        });
  }
  handleAddTimeToproject = async (id) => {
    var event = new Date(this.state.startDate);

    let date = JSON.stringify(event);
    date = date.slice(1, 11);
    console.log(date);
    console.log("time event");
    console.log(id)
    try {
      const article = {
        due_date: `${date}`,
      };
      const headers = {
        Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
        "Content-Type": "application/json",
        "X-Request-Id": `${uuidv4()}`,
      };
      const response = await axios.post(
        `https://api.todoist.com/rest/v1/tasks/${id}`,
        article,
        { headers }
      );
      this.setState({
        data: [...this.state.data, response.data],
      });
    } catch (error) {
      console.error(error);
    }
  };
  handleChange = (e) => {
    this.setState({
      startDate: e,
    });
  };
  handleShowCompletedTask = () => {
    this.setState({
      displayShowCompletedTask: !this.state.displayShowCompletedTask,
    });
    // console.log(this.state.displayShowCompletedTask);
  };

  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer 3e4f91ae7357da3ac437be452f66037df2b8befe",
      },
    };
    axios
      .get(`https://api.todoist.com/rest/v1/tasks`, options)
      .then((resp) => this.setState({ data: resp.data, isLoaded: true }));
  }
  render() {
    const displayActiveTaks = this.handleActivetask();
    const displayCompletedTask = this.handleCompletedtask();

    // console.log(displayActiveTaks);
    // console.log(this.props.match.url.slice(1));
    // console.log(this.state.data);
    return (
      <div className="list-item-container">
        <div className="d-flex mt-5 mb-3">
          <h3>{this.props.location.item.data.each.name}</h3>
          <Dropdown className="delete-button">
            <Dropdown.Toggle
              className="toggle-background-color"
              id="dropdown-basic"
            >
              . . .
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>Edit project</Dropdown.Item>
              <hr />
              {/* <Dropdown.Item>Add section</Dropdown.Item>
              <hr />
              <Dropdown.Item>Import from template</Dropdown.Item>
              <Dropdown.Item>Export as a template</Dropdown.Item>
              <hr />
              <Dropdown.Item>Duplicate project</Dropdown.Item>
              <Dropdown.Item>Email tasks to this project</Dropdown.Item>
              // <Dropdown.Item>Project calendar feed</Dropdown.Item> */}
              {/* // <hr /> */}
              <Dropdown.Item onClick={this.handleShowCompletedTask}>
                {this.state.displayShowCompletedTask
                  ? "Hide completed tasks"
                  : "Show completed tasks"}{" "}
              </Dropdown.Item>
              <hr />
              <Dropdown.Item>Archive project</Dropdown.Item>
              <Dropdown.Item>
                <Modal
                  show={this.state.deleteshow}
                  onHide={this.handleDeleteClose}
                  animation={false}
                >
                  <Modal.Header closeButton>
                    <Modal.Title></Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <h3>Are you sure you want to delete siva?</h3>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button
                      variant="secondary"
                      onClick={this.handleDeleteClose}
                    >
                      Cancel
                    </Button>
                    <Button
                      variant="danger"
                      onClick={() => this.props.handleDelete()}
                    >
                      Delete
                    </Button>
                  </Modal.Footer>
                </Modal>
                <p className="popup-button" onClick={this.handleDeleteShow}>
                  Delete
                </p>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
        {displayActiveTaks.length !== 0 ? (
          <ul className="ul-container">
            {displayActiveTaks.map((each) => {
              return (
                <div>
                  {this.state.showEditTaskId === each.id ? (
                    <div>
                      {/* <h1>Show edit task success</h1> */}
                      <div>
                        <div>
                          <div className="input-task-container">
                            <input
                              onChange={this.handleTaskInput}
                              className="add-task-input"
                              type="text"
                              value={this.state.inputValue}
                              placeholder="Task name"
                            />
                          </div>
                          <div>
                            {/* <div> */}
                              {/* <DateComponent/> */}
                              {/* <Modal
                                show={this.state.show}
                                onHide={this.handleClose}
                                animation={false}
                              >
                                <Modal.Header closeButton>
                                  <Modal.Title>Add Date</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                  <DateComponent
                                    startDate={this.state.startDate}
                                    handleChange={this.handleChange}
                                  />
                                </Modal.Body> */}
                                {/* <Modal.Footer>
                                  <Button
                                    variant="secondary"
                                    onClick={this.handleClose}
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    variant="primary"
                                    onClick={() =>
                                      this.handleAddTimeToproject()
                                    }
                                  >
                                    Save
                                  </Button>
                                </Modal.Footer> */}
                              {/* </Modal> */}
                              {/* <svg
                                className="popup-button button-add"
                                onClick={this.handleShow}
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                fill="currentColor"
                                class="bi bi-calendar2-event"
                                viewBox="0 0 16 16"
                              >
                                <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                              </svg> */}
                              {/* Due date
                            </div> */}
                          </div>
                          <div>
                            <button
                              onClick={() =>
                                this.handleSaveTask(
                                  each.id
                                )
                              }
                              className="bg-danger button-1"
                            >
                              Save task
                            </button>
                            <button
                              className="button-2"
                              onClick={this.handleCloseEditTask}
                            >
                              Cancel
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="d-flex">
                      <div style={{ width: "100%" }}>
                        <li className="list-task">
                          <input
                            onChange={() => this.handleCheckbox(each.id)}
                            value="this.state.inputValue"
                            className="checkbox-circle"
                            id="checkbox"
                            // defaultChecked="!this.state.isChecked"
                            checked={each.completed}
                            type="checkBox"
                          />
                          <label
                            className={
                              each.completed ? "check-box strike" : "check-box"
                            }
                            htmlFor="checkbox"
                          >
                            {each.content}
                            <br />
                            {/* {each.due.date} */}
                          </label>
                        </li>
                        <hr />
                      </div>
                      <div>
                        {/* <DateComponent/> */}
                        <Modal
                          show={this.state.show}
                          onHide={this.handleClose}
                          animation={false}
                        >
                          <Modal.Header closeButton>
                            <Modal.Title>Add Date</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <DateComponent
                              startDate={this.state.startDate}
                              handleChange={this.handleChange}
                            />
                          </Modal.Body>
                          <Modal.Footer>
                            <Button
                              variant="secondary"
                              onClick={this.handleClose}
                            >
                              Cancel
                            </Button>
                            <Button
                              variant="primary"
                              onClick={() =>
                                this.handleAddTimeToproject(each.id)
                              }
                            >
                              Add
                            </Button>
                          </Modal.Footer>
                        </Modal>
                        <svg
                          className="popup-button button-add"
                          onClick={this.handleShow}
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          class="bi bi-calendar2-event"
                          viewBox="0 0 16 16"
                        >
                          <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
                          <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                          <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                        </svg>
                      </div>
                      <div>
                        {" "}
                        <Dropdown className="delete-button">
                          <Dropdown.Toggle
                            className="toggle-background-color"
                            id="dropdown-basic"
                          >
                            . . .
                          </Dropdown.Toggle>

                          <Dropdown.Menu>
                            {/* <Dropdown.Item>Add task above</Dropdown.Item>
                            <Dropdown.Item>Add task below</Dropdown.Item> */}
                            <Dropdown.Item
                              onClick={() => this.handleShowEditTask(each)}
                            >
                              Edit task
                            </Dropdown.Item>
                            <hr />
                            {/* <Dropdown.Item>Remainders</Dropdown.Item>
                            <hr />
                            <Dropdown.Item>Duplicate project</Dropdown.Item>
                            <Dropdown.Item>Move to project</Dropdown.Item>
                            <hr /> */}
                            <Dropdown.Item>
                              <Modal
                                show={this.state.deleteTaskshow}
                                onHide={this.handleDeleteTaskClose}
                                animation={false}
                              >
                                <Modal.Header closeButton>
                                  <Modal.Title></Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                  <h3>Are you sure you want to delete siva?</h3>
                                </Modal.Body>
                                <Modal.Footer>
                                  <Button
                                    variant="secondary"
                                    onClick={this.handleDeleteTaskClose}
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    variant="danger"
                                    onClick={() =>
                                      this.handleDeleteTask(each.id)
                                    }
                                  >
                                    Delete Task
                                  </Button>
                                </Modal.Footer>
                              </Modal>
                              <p
                                className="popup-button"
                                onClick={this.handleDeleteTaskShow}
                              >
                                Delete Task
                              </p>
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </div>
                    </div>
                  )}
                </div>
              );
            })}
          </ul>
        ) : (
          <div>
            <img src="/task.png" alt="" />
          </div>
        )}
        {this.state.displayAddTask ? (
          <div>
            <div>
              <div className="input-task-container">
                <input
                  onChange={this.handleTaskInput}
                  className="add-task-input"
                  type="text"
                  value={this.state.inputValue}
                  placeholder="Task name"
                />
              </div>
              <div>
                <div>
                  {/* <DateComponent/> */}
                  {/* <Modal
                    show={this.state.show}
                    onHide={this.handleClose}
                    animation={false}
                  >
                    <Modal.Header closeButton>
                      <Modal.Title>Add Date</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <DateComponent
                        startDate={this.state.startDate}
                        handleChange={this.handleChange}
                      />
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.handleClose}>
                        Cancel
                      </Button>
                      <Button
                        variant="primary"
                        onClick={() => this.handleAddTimeToproject()}
                      >
                        Add
                      </Button>
                    </Modal.Footer>
                  </Modal> */}
                  {/* <svg
                    className="popup-button button-add"
                    onClick={this.handleShow}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    class="bi bi-calendar2-event"
                    viewBox="0 0 16 16"
                  >
                    <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                    <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                  </svg>
                  Due date */}
                </div>
              </div>
              <div>
                <button
                  onClick={() =>
                    this.handleAddTask(this.props.location.item.data.each.id)
                  }
                  className="bg-danger button-1"
                >
                  Add task
                </button>
                <button className="button-2" onClick={this.handleCloseAddTask}>
                  Cancel
                </button>
              </div>
            </div>
          </div>
        ) : (
          <>
            <div className="d-flex">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-plus-lg"
                viewBox="0 0 16 16"
              >
                <path d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z" />
              </svg>
              <p onClick={this.handleShowAddTask}>Add task</p>
            </div>
            {this.state.displayShowCompletedTask ? (
              <div>
                <ul className="ul-container">
                  {displayCompletedTask.map((each) => {
                    return (
                      <div className="d-flex">
                        <div style={{ width: "100%" }}>
                          <li className="list-task">
                            <input
                              onChange={() => this.handleCheckbox(each.id)}
                              value="this.state.inputValue"
                              className="checkbox-circle"
                              id="checkbox"
                              // defaultChecked="!this.state.isChecked"
                              checked={each.completed}
                              type="checkBox"
                            />
                            <label
                              className={
                                each.completed
                                  ? "check-box strike"
                                  : "check-box"
                              }
                              htmlFor="checkbox"
                            >
                              {each.content}
                            </label>
                          </li>
                          <hr />
                        </div>
                        <div>
                          {" "}
                          <Dropdown className="delete-button">
                            <Dropdown.Toggle
                              className="toggle-background-color"
                              id="dropdown-basic"
                            >
                              . . .
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                              {/* <Dropdown.Item>Add task above</Dropdown.Item>
                              <Dropdown.Item>Add task below</Dropdown.Item> */}
                              <Dropdown.Item>Edit task</Dropdown.Item>
                              <hr />
                              {/* <Dropdown.Item>Remainders</Dropdown.Item>
                              <hr />
                              <Dropdown.Item>Duplicate project</Dropdown.Item>
                              <Dropdown.Item>Move to project</Dropdown.Item>
                              <hr /> */}
                              <Dropdown.Item>
                                <Modal
                                  show={this.state.deleteTaskshow}
                                  onHide={this.handleDeleteTaskClose}
                                  animation={false}
                                >
                                  <Modal.Header closeButton>
                                    <Modal.Title></Modal.Title>
                                  </Modal.Header>
                                  <Modal.Body>
                                    <h3>
                                      Are you sure you want to delete siva?
                                    </h3>
                                  </Modal.Body>
                                  <Modal.Footer>
                                    <Button
                                      variant="secondary"
                                      onClick={this.handleDeleteTaskClose}
                                    >
                                      Cancel
                                    </Button>
                                    <Button
                                      variant="danger"
                                      onClick={() =>
                                        this.handleDeleteTask(each.id)
                                      }
                                    >
                                      Delete Task
                                    </Button>
                                  </Modal.Footer>
                                </Modal>
                                <p
                                  className="popup-button"
                                  onClick={this.handleDeleteTaskShow}
                                >
                                  Delete Task
                                </p>
                              </Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                    );
                  })}
                </ul>
              </div>
            ) : null}
          </>
        )}
      </div>
    );
  }
}

export default RightSideComponent;
