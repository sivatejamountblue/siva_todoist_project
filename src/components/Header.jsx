import React, { Component } from "react";
import { Navbar, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
class Header extends Component {
  render() {
    const filteredSearch = this.props.handleFilteredTasks()
    return (
      <>
        <Navbar style={{ backgroundColor: "#db4c3f" }}>
          <Container>
            <Navbar.Brand>
              <div className="d-flex ">
                <div className="mx-2">
                  <svg
                    onClick={this.props.handleHideLeftComponent}
                    xmlns="http://www.w3.org/2000/svg"
                    width="25"
                    height="25"
                    fill="white"
                    className="bi bi-list"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
                    />
                  </svg>
                </div>
                <div className="mx-2">
                  <Link to="/">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25"
                      height="25"
                      fill="white"
                      className="bi bi-house"
                      viewBox="0 0 16 16"
                    >
                      <path
                        d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"
                      />
                      <path
                        d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"
                      />
                    </svg>
                  </Link>
                </div>
                <div className="mx-2">
                  <input className="input-search" 
                  onClick={this.props.handleInputElClick}
                  onChange={this.props.handleSearchFIlter} 
                  value={this.props.searchInputValue} 
                  placeholder="Search" type="text" />
                  {this.props.filterShow?
                  <ul>
                    {filteredSearch.map((each)=> <li>{each.name}</li>)}
                  </ul>:null}
                </div>
              </div>
            </Navbar.Brand>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default Header;
