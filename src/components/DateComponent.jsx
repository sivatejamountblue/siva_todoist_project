import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
class DateComponent extends Component {
 
 
  render() {
    return (
      <div>
        <DatePicker
          selected={this.props.startDate}
          onChange={this.props.handleChange}
          dateFormat={"yyyy/MM/dd"}
        />
      </div>
    );
  }
}

export default DateComponent;
