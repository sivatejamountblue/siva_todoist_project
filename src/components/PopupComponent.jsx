import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
class PopupComponent extends Component {
  
  render() {
    // console.log(this.props)
    return (
      <div>
        <Modal
          show={this.props.show}
          onHide={this.props.handleClose}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add project</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>Name</h3>
            <input onChange={this.props.handleChange} className="text-input" type="text" />
            <h3>Color</h3>
            <select
              className="color-change"
              onChange={this.props.hanldeColor}
              ref={(input) => (this.menu = input)}
            >
              <option value="47">Charcoal</option>
              <option value="30">Berry Red</option>
              <option value="31">Red</option>
              <option value="36">Green</option>
              <option value="32">Orange</option>
              <option value="48">Grey</option>
            </select>
            <br />
            <div className="d-flex mt-3">
              <label className="switch">
                <input
                  type="checkbox"
                  onClick={this.props.handleAddtofavorite}
                />
                <span className="slider round"></span>
              </label>
              <p className="favorite-text">Add to favorites</p>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={this.props.handleAddproject}>
              Add
            </Button>
          </Modal.Footer>
        </Modal>
        <button
          className="popup-button button-add"
          onClick={this.props.handleShow}
        >
          +
        </button>
      </div>
    );
  }
}

export default PopupComponent;
